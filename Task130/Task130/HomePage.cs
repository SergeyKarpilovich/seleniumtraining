﻿using OpenQA.Selenium;

namespace TestFac
{
    public class HomePage
    {
        private static string HomePageUrl = "http://tut.by";

        private IWebDriver Driver { get; set; }
        private IWebElement LoginField { get { return Driver.FindElement(By.XPath("//input[@name='login']")); } }
        private IWebElement PasswordField { get { return Driver.FindElement(By.XPath("//input[@name='password']")); } }
        private IWebElement LoginButton { get { return Driver.FindElement(By.CssSelector("form.auth-form input[type = submit]")); } }
        public IWebElement EnterAccountLink { get { return Driver.FindElement(By.CssSelector("#authorize a.enter")); } }
        public IWebElement LogOutButton { get { return Driver.FindElement(By.XPath("//a[@class='button wide auth__reg']")); } }
        public IWebElement UserName { get { return Driver.FindElement(By.CssSelector("#authorize span.uname")); } }
        public string GetUserNameText { get { return UserName.Text; } }
        public string EnterAccountLinkText { get { return EnterAccountLink.Text; } }

        public HomePage(IWebDriver driver)
        {
            Driver = driver;
        }

        public void Open()
        {
            Driver.Navigate().GoToUrl(HomePageUrl);
        }

        public void EnterAccount()
        {
            EnterAccountLink.Click();
        }

        private void TypePassword(string password)
        {
            PasswordField.Click();
            PasswordField.Clear();
            PasswordField.SendKeys(password);
        }

        public void LogOut()
        {
            LogOutButton.Click();
        }

        public void ClickOnUserName()
        {
            UserName.Click();
        }

        private void TypeLogin(string login)
        {
            LoginField.Click();
            LoginField.Clear();
            LoginField.SendKeys(login);
        }

        public HomePage LoginWithCorrectCreds(string login, string password)
        {
            TypeLogin(login);
            TypePassword(password);
            LoginButton.Click();

            return new HomePage(Driver);
        }
    }
}