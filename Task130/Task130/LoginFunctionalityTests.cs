﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using Allure.NUnit.Attributes;
using NUnit.Framework.Interfaces;
using Allure.Commons;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Firefox;

namespace TestFac
{
    [TestFixture("Chrome")]
    [TestFixture("Firefox")]
    [AllureSuite("Login Feature Tests")]
    public class LoginFunctionalityTests : AllureReport
    {
        private IWebDriver _driver;
        private HomePage _homePage;
        private HomePage _afterLogin;
        private string _browser;

        public LoginFunctionalityTests(string browser)
        {
            _browser = browser;
        }

        public void SetScreen()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                AllureLifecycle.Instance.AddAttachment("Failed test screenshot", AllureLifecycle.AttachFormat.ImagePng, _driver.TakeScreenshot().AsByteArray);
            }
        }

        [SetUp]
        public void Initialize()
        {

            if (_browser == "Chrome")
            {
                ChromeOptions coptions = new ChromeOptions();
                _driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), coptions.ToCapabilities(), TimeSpan.FromSeconds(600));
            }
            else
            {
                FirefoxOptions foptions = new FirefoxOptions();
                _driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), foptions.ToCapabilities(), TimeSpan.FromSeconds(600));
            }
            
            _driver.Manage().Window.Maximize();
            _homePage = new HomePage(_driver);
            _homePage.Open();
            _homePage.EnterAccount();
        }

        [Test]
        [Parallelizable(ParallelScope.Self)] //run test in parallel
        [AllureSubSuite("Login")]
        [AllureSeverity(Allure.Commons.Model.SeverityLevel.Blocker)]
        [AllureLink("Id 20452")]
        [AllureTest("This test verifies login with correct creds on tut.by")]
        [AllureOwner("Sergey K")]
        public void Login()
        {
            _afterLogin = _homePage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            ElementIsVisible(_afterLogin.UserName, 6, 1);

            Assert.AreEqual("Selenium Test", _afterLogin.GetUserNameText);
        }

        [Test]
        [Parallelizable(ParallelScope.Self)]
        [AllureSubSuite("Log Out")]
        [AllureSeverity(Allure.Commons.Model.SeverityLevel.Blocker)]
        [AllureLink("Id 20452")]
        [AllureTest("This test verifies logout on tut.by")]
        [AllureOwner("Sergey K")]
        public void AccountLogOut()
        {
            _afterLogin = _homePage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            _afterLogin.ClickOnUserName();
            _afterLogin.LogOut();
            ElementIsVisible(_afterLogin.EnterAccountLink, 6, 1);

            Assert.AreEqual("Войти", _afterLogin.EnterAccountLinkText);
        }

        [TearDown]
        public void Quit()
        {
            SetScreen();
            _driver.Quit();
        }

        private bool ElementIsVisible(IWebElement webElement, int seconds, int pollingFreq)
        {
            IClock clock = new SystemClock();
            var wait = new WebDriverWait(clock, _driver, TimeSpan.FromSeconds(seconds), TimeSpan.FromSeconds(pollingFreq));
            var isVisible = wait.Until(condition =>
            {
                try
                {
                    return webElement.Displayed;
                }

                catch (StaleElementReferenceException)
                {
                    return false;
                }

                catch (NoSuchElementException)
                {
                    return false;
                }
            });

            return isVisible;
        }
    }
}