﻿using System;
using OpenQA.Selenium;
using NUnit.Framework;
using Task150.Pages;
using OpenQA.Selenium.Remote;
using Allure.Commons;
using Allure.NUnit.Attributes;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Chrome;

namespace Task150.Tests
{
    [TestFixture("Run locally")]
    [TestFixture("Run in grid")]
    [TestFixture("Run in SauceLabs")]
    [AllureSuite("Delete Email Feature Tests")]
    public class DeleteEmailTests : AllureReport
    {
        private IWebDriver Driver { get; set; }
        private string RunBy;

        public DeleteEmailTests(string runby)
        {
            RunBy = runby;
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            switch (RunBy)
            {
                case ("Run locally"):
                    Driver = new ChromeDriver();
                    break;
                case ("Run in grid"):
                    ChromeOptions options = new ChromeOptions();
                    options.PlatformName = "WIN10";
                    Driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options);
                    break;
                case ("Run in SauceLabs"):
                    string sauceUserName = Environment.GetEnvironmentVariable("SAUCE_USERNAME", EnvironmentVariableTarget.User);
                    string sauceAccessKey = Environment.GetEnvironmentVariable("SAUCE_ACCESS_KEY", EnvironmentVariableTarget.User);
                    ChromeOptions coptions = new ChromeOptions();
                    coptions.AddAdditionalCapability("username", sauceUserName);
                    coptions.AddAdditionalCapability("accessKey", sauceAccessKey);
                    Driver = new RemoteWebDriver(new Uri("http://ondemand.saucelabs.com:80/wd/hub"), coptions.ToCapabilities(),
                    TimeSpan.FromSeconds(600));
                    break;
            }

            Driver.Manage().Window.Maximize();
        }

        [Test]
        [AllureSubSuite("Gmail Delete Email Tests")]
        [AllureSeverity(Allure.Commons.Model.SeverityLevel.Critical)]
        [AllureTest("This test verifies that user can see deleted e-mail in trash folder")]
        [AllureOwner("Sergey K")]
        public void DeleteEmailListedInTrash()
        {
            LoginPage loginPage = new LoginPage(Driver);

            if (!(Driver.Url.StartsWith("https://accounts.google.com")))
            {
                loginPage.Open();
            }
            
            loginPage.EnterUsername("seleniumtests30");			
            InboxPage inboxPage = loginPage.EnterPassword("060788avavav");			
            TrashPage trashPage = inboxPage.GoToTrash();			
            trashPage.DeleteAllFromTrash();			
            InboxPage inbPage = trashPage.ToInbox();			
            inbPage.DeleteEmail();
            TrashPage trPage = inbPage.GoToTrash();
            
            Assert.That(trPage.EmptyTrash.Displayed, "Message is not deleted!");
        }

        [TearDown]
        public void CleanUp()
        {
            Driver.Manage().Cookies.DeleteAllCookies();
            Driver.Navigate().Refresh();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            SetScreen();
            Driver?.Quit();
        }

        public void SetScreen()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                AllureLifecycle.Instance.AddAttachment("Failed test screenshot", AllureLifecycle.AttachFormat.ImagePng, Driver.TakeScreenshot().AsByteArray);
            }
        }

    }
}