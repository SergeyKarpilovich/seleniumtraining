﻿using System;
using OpenQA.Selenium;
using NUnit.Framework;
using Task150.Pages;
using OpenQA.Selenium.Chrome;
using Allure.NUnit.Attributes;
using Allure.Commons;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Remote;

namespace Task150.Tests
{
    [TestFixture("Run locally")]
    [TestFixture("Run in grid")]
    [TestFixture("Run in SauceLabs")]
    [AllureSuite("Send Email Feature Tests")]
    public class SendEmailsTests : AllureReport
    {
        private IWebDriver Driver { get; set; }
        private string RunBy;

        public SendEmailsTests(string runby)
        {
            RunBy = runby;
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            switch(RunBy)
            {
                case ("Run locally"):
                    Driver = new ChromeDriver();
                    break;
                case ("Run in grid"):
                    ChromeOptions options = new ChromeOptions();
                    options.PlatformName = "WIN10";
                    Driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options);
                    break;
                case ("Run in SauceLabs"):
                    string sauceUserName = Environment.GetEnvironmentVariable("SAUCE_USERNAME", EnvironmentVariableTarget.User);
                    string sauceAccessKey = Environment.GetEnvironmentVariable("SAUCE_ACCESS_KEY", EnvironmentVariableTarget.User);
                    ChromeOptions coptions = new ChromeOptions();
                    coptions.AddAdditionalCapability("username", sauceUserName);
                    coptions.AddAdditionalCapability("accessKey", sauceAccessKey);
                    Driver = new RemoteWebDriver(new Uri("http://ondemand.saucelabs.com:80/wd/hub"), coptions.ToCapabilities(),
                    TimeSpan.FromSeconds(600));
                    break;
            }
            
            Driver.Manage().Window.Maximize();
        }

        [Test]
        [AllureSubSuite("Gmail Send Email Tests")]
        [AllureSeverity(Allure.Commons.Model.SeverityLevel.Blocker)]
        [AllureTest("This test verifies that user can send and receive e-mails")]
        [AllureOwner("Sergey K")]
        public void SendEmailAbility()
        {
            LoginPage loginPage = new LoginPage(Driver);
            if (!(Driver.Url.StartsWith("https://accounts.google.com")))
            {
                loginPage.Open();
            }
            loginPage.ChangeLogin();
            loginPage.EnterUsername("seleniumtests10");         
            InboxPage inboxPage = loginPage.EnterPassword("060788avavav");
            inboxPage.OpenMessageForm();
            inboxPage.SendEmail("seleniumtests30@gmail.com", "Test Selenium", "Test Selenium");
            inboxPage.OpenUserProfile();
            inboxPage.SignOut();
            Driver.Manage().Cookies.DeleteAllCookies();
            Driver.Navigate().Refresh();
            
            LoginPage lp = new LoginPage(Driver);
            if (!(Driver.Url.StartsWith("https://accounts.google.com")))
            {
                lp.Open();
            }
            lp.ChangeLogin();
            lp.EnterUsername("seleniumtests30");
            InboxPage inbPage = loginPage.EnterPassword("060788avavav");
            inbPage.WaitForReceivedMessage();

            Assert.IsTrue(inbPage.ReceivedMessage.Enabled);

            inbPage.OpenUserProfile();
            inbPage.SignOut();
        }

        [Test]
        [AllureSubSuite("Gmail Send Email Tests")]
        [AllureSeverity(Allure.Commons.Model.SeverityLevel.Blocker)]
        [AllureTest("This test verifies that user can see his sent email in Sent Folder")]
        [AllureOwner("Sergey K")]
        public void SentEmailFolderTest()
        {
            LoginPage loginPage = new LoginPage(Driver);
            if (!(Driver.Url.StartsWith("https://accounts.google.com")))
            {
                loginPage.Open();
            }
            loginPage.ChangeLogin();            
            loginPage.EnterUsername("seleniumtests10");           
            InboxPage inboxPage = loginPage.EnterPassword("060788avavav");           
            inboxPage.OpenMessageForm();           
            inboxPage.SendEmail("seleniumtests50@gmail.com", "SentEmailFolderTest", "Test Selenium");            
            SendFolder sendFolder = inboxPage.OpenSentFolder();
            sendFolder.WaitForSentMessageTitle();

            Assert.That(sendFolder.MessageTitle.Enabled);
        }

        [TearDown]
        public void CleanUp()
        {
            Driver.Manage().Cookies.DeleteAllCookies();
            Driver.Navigate().Refresh();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            SetScreen();
            Driver?.Quit();
        }

        public void SetScreen()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                AllureLifecycle.Instance.AddAttachment("Failed test screenshot", AllureLifecycle.AttachFormat.ImagePng, Driver.TakeScreenshot().AsByteArray);
            }
        }

    }
}