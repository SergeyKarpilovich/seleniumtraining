﻿using System;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium;
using Task150.Pages;
using OpenQA.Selenium.Firefox;
using Allure.Commons;
using Allure.NUnit.Attributes;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Remote;
using System.Collections.Generic;
using Objectivity.Test.Automation.Tests.NUnit.DataDriven;

namespace Task150.Tests
{
    [TestFixture("Run locally")]
    [TestFixture("Run in grid")]
    [TestFixture("Run in SauceLabs")]
    [AllureSuite("Login Feature Tests")]
    public class AuthorizationTests : AllureReport
    {
        private IWebDriver Driver { get; set; }
        private string RunBy;

        public AuthorizationTests(string runby)
        {
            RunBy = runby;
        }

        [OneTimeSetUp]
        public void SetUp()
        {
            Driver = new FirefoxDriver();
            switch (RunBy)
            {
            case ("Run locally"):
                Driver = new FirefoxDriver();
                break;
            case ("Run in grid"):
                ChromeOptions options = new ChromeOptions();
                options.PlatformName = "WIN10";
                Driver = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), options);
                break;
            case ("Run in SauceLabs"):
                string sauceUserName = Environment.GetEnvironmentVariable("SAUCE_USERNAME", EnvironmentVariableTarget.User);
                string sauceAccessKey = Environment.GetEnvironmentVariable("SAUCE_ACCESS_KEY", EnvironmentVariableTarget.User);
                ChromeOptions coptions = new ChromeOptions();
                coptions.AddAdditionalCapability("username", sauceUserName);
                coptions.AddAdditionalCapability("accessKey", sauceAccessKey);
                Driver = new RemoteWebDriver(new Uri("http://ondemand.saucelabs.com:80/wd/hub"), coptions.ToCapabilities(),
                TimeSpan.FromSeconds(600));
                break;
            }

            Driver.Manage().Window.Maximize();
        }

        [Test]
        [TestCaseSource(typeof(TestData), "LoginTest")]
        [AllureSubSuite("Gmail Login/Logout")]
        [AllureSeverity(Allure.Commons.Model.SeverityLevel.Blocker)]
        [AllureTest("This test verifies login with correct creds on gmail.com")]
        [AllureOwner("Sergey K")]
        public void LoginTest(IDictionary<string, string> parameters)
        {
            Driver = new FirefoxDriver();
            LoginPage loginPage = new LoginPage(Driver);
            if (!(Driver.Url.StartsWith("https://accounts.google.com")))
            {
                loginPage.Open();
            }
            loginPage.ChangeLogin();
            loginPage.EnterUsername(parameters["userName"]);           
            InboxPage inboxPage = loginPage.EnterPassword(parameters["password"]);
            inboxPage.OpenUserProfile();

            Assert.That(inboxPage.Logo.Displayed, "Log in failed. Member Profile Logo is not displayed.");

            inboxPage.SignOut();
        }

        [Test]
        [TestCaseSource(typeof(TestData), "LogoutTest")]
        [AllureSubSuite("Gmail Login/Logout")]
        [AllureSeverity(Allure.Commons.Model.SeverityLevel.Blocker)]
        [AllureTest("This test verifies logout on gmail.com")]
        [AllureOwner("Sergey K")]
        public void Logout(IDictionary<string, string> parameters)
        {
            LoginPage loginPage = new LoginPage(Driver);
            if (!(Driver.Url.StartsWith("https://accounts.google.com")))
            {
                loginPage.Open();
            }		
            loginPage.ChangeLogin();
            loginPage.EnterUsername(parameters["userName"]);
            InboxPage inboxPage = loginPage.EnterPassword(parameters["password"]);
            inboxPage.OpenUserProfile();
            LoginPage lp = inboxPage.SignOut();
            lp.WaitForLogo();

            Assert.That(lp.LoginPageTitle.Displayed, "Log out failed. Google Login Page Title is not displayed.");
        }

        [TearDown]
        public void CleanUp()
        {
            Driver.Manage().Cookies.DeleteAllCookies();
            Driver.Navigate().Refresh();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            SetScreen();
            Driver?.Quit();
        }

        public void SetScreen()
        {
            if (TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed)
            {
                AllureLifecycle.Instance.AddAttachment("Failed test screenshot", AllureLifecycle.AttachFormat.ImagePng, Driver.TakeScreenshot().AsByteArray);
            }
        }       

    }
}