﻿using OpenQA.Selenium;

namespace Task150.Pages
{
    public class SendFolder : BasePage
    {
        public IWebElement SentEmail => Driver.FindElement(By.CssSelector("colgroup+tbody>tr:first-child"));
        public IWebElement SentEmailContent => Driver.FindElement(By.CssSelector(".a3s.aXjCH>div:first-child"));
        public IWebElement MessageTitle => Driver.FindElement(By.XPath("//span[text()='SentEmailFolderTest']"));

        public SendFolder(IWebDriver driver) : base(driver)
        {
        }

        public void WaitForSentMessageTitle()
        {
            Delay(5000);
        }

    }
}