﻿using OpenQA.Selenium;

namespace Task150.Pages
{
    public class LoginPage : BasePage
    {
        public IWebElement LoginField => Driver.FindElement(By.Id("identifierId"));
        private IWebElement ProceedButton => Driver.FindElement(By.XPath("//span[text()='Далее']"));
        public IWebElement PasswordField => Driver.FindElement(By.XPath("//input[@type='password']"));
        public IWebElement Proceed => Driver.FindElement(By.CssSelector("span.RveJvd.snByac"));
        public IWebElement LoginPageTitle => Driver.FindElement(By.Id("logo"));
        public IWebElement ResetLogin => Driver.FindElement(By.Id("identifierLink"));

        public LoginPage(IWebDriver driver) : base(driver)
        {
        }

        public void Open()
        {
            Driver.Navigate().GoToUrl(_baseURL);
        }

        public void EnterUsername(string login)
        {
            ElementIsVisible(LoginField, 10, 1);
            LoginField.Click();
            LoginField.Clear();
            LoginField.SendKeys(login);
            ElementIsVisible(ProceedButton, 10, 1);
            ProceedButton.Click();
        }

        public InboxPage EnterPassword(string password)
        {
            WaitForElement(By.XPath("//input[@type='password']"));		
            PasswordField.Click();
            PasswordField.Clear();
            PasswordField.SendKeys(password);
            
            ElementIsVisible(Proceed, 10, 1);
            Proceed.Click();
            return new InboxPage(Driver);
        }

        public void WaitForLogo()
        {
            WaitForElement(By.Id("logo"));
        }

        public void ChangeLogin()
        {
            if (Driver.FindElements(By.Id("identifierLink")).Count > 0)
            {
                ResetLogin.Click();
            }

        }

    }
}