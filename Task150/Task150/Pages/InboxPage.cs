﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace Task150.Pages
{
    public class InboxPage : BasePage
    {
        public IWebElement AccountLogo => Driver.FindElement(By.CssSelector("a.gb_b.gb_hb.gb_R"));
        public IWebElement Logo => Driver.FindElement(By.CssSelector("a.gb_Bb.gb_sg"));
        public IWebElement SignOutButton => Driver.FindElement(By.Id("gb_71"));
        public IWebElement EmailCheckbox => Driver.FindElement(By.CssSelector("div[class=Cp] div[role=checkbox]:first-child"));
        public IWebElement DeleteIcon => Driver.FindElement(By.CssSelector("div[aria-label=Delete]"));
        public IWebElement TrashIcon => Driver.FindElement(By.CssSelector("a[title=Trash]"));
        public IWebElement SendEmailButton => Driver.FindElement(By.CssSelector("div[gh=cm]"));
        public IWebElement MessageTo => Driver.FindElement(By.CssSelector("textarea[name=to]"));
        public IWebElement MessageSubject => Driver.FindElement(By.CssSelector("input[name=subjectbox]"));
        public IWebElement MessageContent => Driver.FindElement(By.CssSelector("div[aria-label='Message Body']"));
        public IWebElement SendButton => Driver.FindElement(By.CssSelector(".T-I.J-J5-Ji.aoO.T-I-atl.L3"));
        public IWebElement SendFolder => Driver.FindElement(By.CssSelector("a[title = Sent]"));
        public IWebElement ReceivedMessage => Driver.FindElement(By.XPath("//span[text()='Test Selenium']"));
        

        public InboxPage(IWebDriver driver) : base(driver)
        {
        }

        public void OpenUserProfile()
        {
            ElementIsVisible(AccountLogo, 10, 1);
            AccountLogo.Click();
        }

        public LoginPage SignOut()
        {
            ElementIsVisible(SignOutButton, 10, 1);
            SignOutButton.Click();
            return new LoginPage(Driver);
        }

        public void OpenMessageForm()
        {
            WaitForElement(By.CssSelector("div[gh=cm]"));
            SendEmailButton.Click();
        }

        public void SendEmail(string email, string subject, string message)
        {
            Driver.SwitchTo().ActiveElement();
            Delay(5000); //needed
            WaitForElement(By.CssSelector("textarea[name=to]"));
            MessageTo.Click();						
            MessageTo.SendKeys(email);			
            WaitForElement(By.CssSelector("input[name=subjectbox]"));
            MessageSubject.SendKeys(subject);
            MessageContent.Click();
            MessageContent.SendKeys(message);
            WaitForElement(By.CssSelector(".T-I.J-J5-Ji.aoO.T-I-atl.L3"));
            SendButton.Click();
            Delay(5000);
        }

        public SendFolder OpenSentFolder()
        {
            Delay(3000);
            SendFolder.Click();
            return new SendFolder(Driver);
        }

        public TrashPage GoToTrash()
        {
            Delay(3000);
            TrashIcon.Click();
            return new TrashPage(Driver);
        }

        public void DeleteEmail()
        {
            ElementIsVisible(EmailCheckbox, 10, 1);
            Actions actions = new Actions(Driver);
            actions.DragAndDrop(EmailCheckbox, TrashIcon).Build().Perform();
        }

        public void WaitForReceivedMessage()
        {
            Delay(5000);
        }
    }
}