﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Task150.Pages
{
    public class TrashPage : BasePage
    {
        public IWebElement EmptyTrash => Driver.FindElement(By.CssSelector(".x2"));
        //public IWebElement NoConvInTrash => Driver.FindElement(By.XPath("//td[text()='No conversations in Trash.']"));

        public TrashPage(IWebDriver driver) : base(driver)
        {
        }

        public void DeleteAllFromTrash()
        {
            WaitForElement(By.CssSelector(".x2"));
            EmptyTrash.Click();
            new Actions(Driver).SendKeys(Keys.Enter).Perform();
        }

        public InboxPage ToInbox()
        {
            Delay(5000);
            Driver.Navigate().Back();
            return new InboxPage(Driver);
        }

    }
}