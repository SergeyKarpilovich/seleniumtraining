﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace Task150.Pages
{
    public class BasePage
    {
        protected IWebDriver Driver { get; set; }
        protected static string _baseURL = "https://gmail.com";

        public BasePage(IWebDriver driver)
        {
            Driver = driver;
        }

        protected bool ElementIsVisible(IWebElement webElement, int seconds, int pollingFreq)
        {
            IClock clock = new SystemClock();
            WebDriverWait wait = new WebDriverWait(clock, Driver, TimeSpan.FromSeconds(seconds), TimeSpan.FromSeconds(pollingFreq));
            var isVisible = wait.Until(condition =>
            {
                try
                {
                    return webElement.Displayed;
                }

                catch (StaleElementReferenceException)
                {
                    return false;
                }

                catch (NoSuchElementException)
                {
                    return false;
                }
            });

            return isVisible;
        }

        protected void WaitForElement(By locator)
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(locator));
        }

        protected void Delay(int milis)
        {
            Thread.Sleep(milis);
        }
    }

}