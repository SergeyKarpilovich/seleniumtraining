﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using NUnit.Framework.Interfaces;

namespace TestFac
{
    [TestFixture("Chrome", "40.0", "Linux")]
    [TestFixture("Firefox", "39.0", "Windows 8.1")]
    [TestFixture("MicrosoftEdge", "latest", "Windows 10")]
    public class LoginFunctionalityTests
    {
        private IWebDriver _driver;
        private HomePage _homePage;
        private HomePage _afterLogin;
        private string _browser;
        private string _version;
        private string _os;

        public LoginFunctionalityTests(string browser, string version, string os)
        {
            _browser = browser;
            _version = version;
            _os = os;
        }

        [SetUp]
        public void BeforeTest()
        {
            string sauceUserName = Environment.GetEnvironmentVariable("SAUCE_USERNAME", EnvironmentVariableTarget.User);
            string sauceAccessKey = Environment.GetEnvironmentVariable("SAUCE_ACCESS_KEY", EnvironmentVariableTarget.User);

            switch (_browser) {
                case "Chrome":
                    ChromeOptions chromeOptions = new ChromeOptions();                    
                    SetBrowserOptions(chromeOptions, sauceUserName, sauceAccessKey);
                    break;
                case "Firefox":
                    FirefoxOptions firefoxOptions = new FirefoxOptions();
                    SetBrowserOptions(firefoxOptions, sauceUserName, sauceAccessKey);
                    break;
                default:
                    EdgeOptions edgeOptions = new EdgeOptions();
                    SetBrowserOptions(edgeOptions, sauceUserName, sauceAccessKey);
                    break;
            }		
        }

        [Test]
        public void Login()
        {
            _homePage = new HomePage(_driver);
            _homePage.Open();
            _homePage.EnterAccount();
            _afterLogin = _homePage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            ElementIsVisible(_afterLogin.UserName, 6, 1);
            Assert.AreEqual("Selenium Test", _afterLogin.GetUserNameText, $"Expected username value: Selenium Test\n Actual username value: {_afterLogin.GetUserNameText}");
        }

        [Test]
        public void AccountLogOut()
        {
            _homePage = new HomePage(_driver);
            _homePage.Open();
            _homePage.EnterAccount();
            _afterLogin = _homePage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            _afterLogin.ClickOnUserName();
            _afterLogin.LogOut();
            ElementIsVisible(_afterLogin.EnterAccountLink, 6, 1);

            Assert.AreEqual("Войти", _afterLogin.EnterAccountLinkText);
        }

        [TearDown]
        public void Quit()
        {
            var passed = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Passed;
            ((IJavaScriptExecutor)_driver).ExecuteScript("sauce:job-result=" + (passed ? "passed" : "failed"));
            if (_driver != null)
                _driver.Quit();
        }

        private bool ElementIsVisible(IWebElement webElement, int seconds, int pollingFreq)
        {
            IClock clock = new SystemClock();
            var wait = new WebDriverWait(clock, _driver, TimeSpan.FromSeconds(seconds), TimeSpan.FromSeconds(pollingFreq));
            var isVisible = wait.Until(condition =>
            {
                try
                {
                    return webElement.Displayed;
                }

                catch (StaleElementReferenceException)
                {
                    return false;
                }

                catch (NoSuchElementException)
                {
                    return false;
                }
            });

            return isVisible;
        }

        private void SetBrowserOptions(DriverOptions options, string sauceUserName, string sauceAccessKey)
        {
            options.AddAdditionalCapability(CapabilityType.Version, _version);
            options.AddAdditionalCapability(CapabilityType.Platform, _os);
            options.AddAdditionalCapability("username", sauceUserName);
            options.AddAdditionalCapability("accessKey", sauceAccessKey);
            _driver = new RemoteWebDriver(new Uri("http://ondemand.saucelabs.com:80/wd/hub"), options.ToCapabilities(),
            TimeSpan.FromSeconds(600));
        }
    }
}