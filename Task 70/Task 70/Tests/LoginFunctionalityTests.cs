﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;

namespace TestFac
{
    [TestFixture]
    public class LoginFunctionalityTests
    {
        private IWebDriver _driver;
        private HomePage _homePage;
        private HomePage _afterLogin;

        [SetUp]
        public void Initialize()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _homePage = new HomePage(_driver);
            _homePage.Open();
            _homePage.EnterAccount();
        }

        [Test]
        public void Login()
        {           
            _afterLogin = _homePage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            ElementIsVisible(_afterLogin.UserName, 6, 1);

            Assert.AreEqual("Selenium Test", _afterLogin.GetUserNameText, $"Expected username value: Selenium Test\n Actual username value: {_afterLogin.GetUserNameText}");
        }

        [Test]
        public void AccountLogOut()
        {
            _afterLogin = _homePage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            _afterLogin.ClickOnUserName();
            _afterLogin.LogOut();
            ElementIsVisible(_afterLogin.EnterAccountLink, 6, 1);

            Assert.AreEqual("Войти", _afterLogin.EnterAccountLinkText);
        }

        [TearDown]
        public void Quit()
        {
            _driver.Quit();
        }

        private bool ElementIsVisible(IWebElement webElement, int seconds, int pollingFreq)
        {
            IClock clock = new SystemClock();
            var wait = new WebDriverWait(clock, _driver, TimeSpan.FromSeconds(seconds), TimeSpan.FromSeconds(pollingFreq));
            var isVisible = wait.Until(condition =>
            {
                try
                {
                    return webElement.Displayed;
                }

                catch (StaleElementReferenceException)
                {
                    return false;
                }

                catch (NoSuchElementException)
                {
                    return false;
                }
            });

            return isVisible;
        }
    }
}