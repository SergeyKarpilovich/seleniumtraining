﻿using OpenQA.Selenium;

namespace Task50
{
    internal class LoginPage : BasePage
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement LoginField => Driver.FindElement(By.XPath("//*[@class='auth-form']//input[@name='login']"));
        private IWebElement PasswordField => Driver.FindElement(By.XPath("//*[@class='auth-form']//input[@name='password']"));
        private IWebElement EnterAccountLink => Driver.FindElement(By.CssSelector("#authorize a.enter"));
        public IWebElement UserName => Driver.FindElement(By.CssSelector("#authorize span.uname"));
        public IWebElement LoginButton => Driver.FindElement(By.CssSelector("form.auth-form input[type = submit]"));
        public string GetUserNameText => UserName.Text;

        public void Open()
        {
            Driver.Navigate().GoToUrl("https://www.tut.by");
        }

        public void EnterAccount()
        {
            EnterAccountLink.Click();
        }

        private void TypePassword(string password)
        {
            PasswordField.Click();
            PasswordField.Clear();
            PasswordField.SendKeys(password);
        }

        private void TypeLogin(string login)
        {
            LoginField.Click();
            LoginField.Clear();
            LoginField.SendKeys(login);
        }

        public LoginPage LoginWithCorrectCreds(string login, string password)
        {
            TypeLogin(login);
            TypePassword(password);
            LoginButton.Click();

            return new LoginPage(Driver);
        }
    }
}
