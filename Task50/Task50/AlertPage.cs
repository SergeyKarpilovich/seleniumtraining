﻿using OpenQA.Selenium;

namespace Task50
{
    public class AlertPage : BasePage
    {
        public IWebElement AlertButton => Driver.FindElement(By.XPath("//button[text()='Click for JS Alert']"));
        public IWebElement ConfirmButton => Driver.FindElement(By.XPath("//button[text()='Click for JS Confirm']"));
        public IWebElement PromptButton => Driver.FindElement(By.XPath("//button[text()='Click for JS Prompt']"));
        public IWebElement Result => Driver.FindElement(By.Id("result"));

        public AlertPage(IWebDriver driver) : base(driver)
        {
        }

        public void Open()
        {
            Driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/javascript_alerts");
        }

        public IAlert SwitchToAlert() //for instantiating alert to use in CloseAlert(), CloseConfirm(), ClosePrompt() methods
        {
            IAlert alert = Driver.SwitchTo().Alert();
            return alert;
        }

        public void CloseAlert() //close JS Alert
        {
            SwitchToAlert().Accept();
        }

        public void CloseConfirm() //close JS Confirm
        {
            SwitchToAlert().Dismiss();
        }

        public void ClosePrompt()
        {
            IAlert prompt = SwitchToAlert();
            prompt.SendKeys("Prompt test");
            prompt.Accept();
        }

        public void ClickButton(IWebElement button)
        {
            button.Click();
        }
    }
}