﻿using OpenQA.Selenium;

namespace Task50
{
    public class FramePage : BasePage
    {
        public FramePage(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement Frame => Driver.FindElement(By.Id("mce_0_ifr"));
        public IWebElement BoldButton => Driver.FindElement(By.XPath("//*[@id='mceu_3']/button"));
        public IWebElement TinyMCE => Driver.FindElement(By.Id("tinymce"));

        public void Open()
        {
            Driver.Navigate().GoToUrl("https://the-internet.herokuapp.com/iframe");
        }

        public void ToFrame()
        {
            Driver.SwitchTo().Frame(this.Frame);
        }

        public void OutOfFrame()
        {
            Driver.SwitchTo().DefaultContent();
        }

        public void TypeText(string notBoldText, string boldText)
        {
            TinyMCE.Click();
            TinyMCE.Clear();
            TinyMCE.SendKeys(notBoldText);
            OutOfFrame();
            BoldButton.Click();
            ToFrame();
            TinyMCE.SendKeys(boldText);
        }
    }
}