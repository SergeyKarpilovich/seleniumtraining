﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Firefox;
using System.Threading;

namespace Task50
{
    [TestFixture]
    public class TestsForTask50
    {
        private IWebDriver _driver;
        static object[] LoginParameters = //use for Data Driven Test
        {
            new string[] {"seleniumtests@tut.by", "123456789zxcvbn", "Selenium Test"},
            new string[] {"seleniumtests2@tut.by", "123456789zxcvbn", "Selenium Test"}
        };

        [SetUp]
        public void Before()
        {
            _driver = new FirefoxDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5); //point 1 of task
            _driver.Manage().Window.Maximize();
        }

        [Test]
        public void LoginTest()
        {
            LoginPage loginPage = new LoginPage(_driver);
            loginPage.Open();
            Thread.Sleep(5000); //point 2 of task - this is explicit wait
            loginPage.EnterAccount();
            LoginPage afterLogin = loginPage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            IClock clock = new SystemClock();
            var wait = new WebDriverWait(clock, _driver, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(1)); //polling frequency as last parameter
            wait.Until(condition => //point 3 of task
            {
                try
                {
                    return afterLogin.UserName.Displayed;
                }

                catch (StaleElementReferenceException)
                {
                    return false;
                }

                catch (NoSuchElementException)
                {
                    return false;
                }
            });

            Assert.AreEqual("Selenium Test", afterLogin.GetUserNameText, $"Expected username value: Selenium Test\n Actual username value: {afterLogin.GetUserNameText}");
        }

        [TestCaseSource("LoginParameters")]
        public void DataDrivenTest(string login, string password, string userName) //point 4 of task
        {
            LoginPage loginPage = new LoginPage(_driver);
            loginPage.Open();
            loginPage.EnterAccount();
            LoginPage afterLogin = loginPage.LoginWithCorrectCreds(login, password);

            Assert.AreEqual($"{userName}", afterLogin.GetUserNameText, $"Expected username value: Selenium Test\n Actual username value: {afterLogin.GetUserNameText}");
        }

        [Test]
        public void FrameTest() //point 5 of task
        {
            FramePage framePage = new FramePage(_driver);
            framePage.Open();
            IClock clock = new SystemClock();
            var wait = new WebDriverWait(clock, _driver, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(1)); //polling frequency as last parameter
            wait.Until(condition =>
            {
                try
                {
                    return framePage.Frame.Displayed;
                }

                catch (StaleElementReferenceException)
                {
                    return false;
                }

                catch (NoSuchElementException)
                {
                    return false;
                }
            });

            framePage.ToFrame();
            framePage.TypeText("Hello ", "world!");

            Assert.AreEqual("Hello \ufeffworld!", framePage.TinyMCE.GetAttribute("textContent")); //include verification that "world!" is bolded
        }

        [Test]
        public void JSAlertTest() //point 6 of task
        {
            AlertPage alertPage = new AlertPage(_driver);
            alertPage.Open();
            alertPage.ClickButton(alertPage.AlertButton);
            alertPage.CloseAlert();

            Assert.IsTrue(alertPage.Result.Text == "You successfuly clicked an alert", $"Expected message: You successfuly clicked an alert; Actual message: {alertPage.Result.Text}");
        }

        [Test]
        public void JSConfirmTest() //point 6 of task
        {
            AlertPage alertPage = new AlertPage(_driver);
            alertPage.Open();
            alertPage.ClickButton(alertPage.ConfirmButton);
            alertPage.CloseConfirm();

            Assert.IsTrue(alertPage.Result.Text == "You clicked: Cancel", $"Expected message: You clicked: Cancel; Actual message: {alertPage.Result.Text}");
        }

        [Test]
        public void JSPromptTest() //point 6 of task
        {
            AlertPage alertPage = new AlertPage(_driver);
            alertPage.Open();
            alertPage.ClickButton(alertPage.PromptButton);
            alertPage.ClosePrompt();

            Assert.IsTrue(alertPage.Result.Text == "You entered: Prompt test", $"Expected message: You entered: Prompt test; Actual message: {alertPage.Result.Text}");
        }

        [TearDown]
        public void Close()
        {
            _driver.Quit();
        }
    }
}