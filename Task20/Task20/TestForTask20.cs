﻿using System;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace Task20
{
    public class TestForTask20
    {
        private IWebDriver driver;
        private By enterAccountLink = By.CssSelector("div#authorize a.enter");
        private By loginField = By.XPath("//form[@class='auth-form']//input[@name='login']");
        private By passwordField = By.XPath("//form[@class='auth-form']//input[@name='password']");
        private By submitCredsButton = By.CssSelector("form.auth-form input[type = submit]");
        private By userNameText = By.CssSelector("div#authorize span.uname");

        [SetUp]
        public void Start()
        {
            driver = new ChromeDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void LoginTest()
        {			
            driver.Navigate().GoToUrl("https://www.tut.by");
            driver.FindElement(enterAccountLink).Click();
            driver.FindElement(loginField).SendKeys("seleniumtests@tut.by");
            driver.FindElement(passwordField).SendKeys("123456789zxcvbn");
            driver.FindElement(submitCredsButton).Submit();
            Thread.Sleep(5000); //implicit wait
            IWebElement userName = driver.FindElement(userNameText);
            Assert.AreEqual("Selenium Test", userName.Text, string.Format("Expected username value: {0}\n Actual username value: {1}", "Selenium Test", userName.Text));
        }

        [TearDown]
        public void Close()
        {
            driver.Quit();
        }

    }
}