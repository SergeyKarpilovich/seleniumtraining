﻿using OpenQA.Selenium;

namespace Task20
{
	public sealed class AllLocatorTypes
	{
		private By byName = By.Name("search");
		private By byId = By.Id("search_from_str");
		private By byClassName = By.ClassName("header-logo");
		private By byTagName = By.TagName("h2");
		private By byLinkText = By.LinkText("Все новости");
		private By byPartialLinkText = By.ClassName("Связаться с");
		private By byCssSelector = By.CssSelector("div#mainmenu a.topbar-burger");
		private By byXpath = By.XPath("//a[text()='Создать резюме']");
	}
}