﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Reflection;

namespace TestFac
{
    [TestFixture]
    public class LoginFunctionalityTests
    {
        private IWebDriver _driver;
        private HomePage _homePage;
        private HomePage _afterLogin;

        private void TakeScreenshot(String fileName) //here is the 80 task
        {
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location); //screenshots are saved in bin/debug folder
            Screenshot screenShot = ((ITakesScreenshot)_driver).GetScreenshot();
            screenShot.SaveAsFile(fileName: $"{outPutDirectory}\\{fileName}{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.png", format: ScreenshotImageFormat.Png);
        }

        [SetUp]
        public void BeforeTest()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _homePage = new HomePage(_driver);
            _homePage.Open();
            _homePage.EnterAccount();
        }

        [Test]
        public void Login()
        {            
            _afterLogin = _homePage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            ElementIsVisible(_afterLogin.UserName, 6, 1);
            TakeScreenshot("Home Page of tut.by after Login with correct creds "); //here is the 80 task
            Assert.AreEqual("Selenium Test", _afterLogin.GetUserNameText, $"Expected username value: Selenium Test\n Actual username value: {_afterLogin.GetUserNameText}");
        }

        [Test]
        public void AccountLogOut()
        {
            _afterLogin = _homePage.LoginWithCorrectCreds("seleniumtests@tut.by", "123456789zxcvbn");
            _afterLogin.ClickOnUserName();
            _afterLogin.LogOut();
            ElementIsVisible(_afterLogin.EnterAccountLink, 6, 1);

            TakeScreenshot("Home Page of tut.by after Logout "); //here is the 80 task
            Assert.AreEqual("Войти", _afterLogin.EnterAccountLinkText);
        }

        [TearDown]
        public void Quit()
        {
            _driver.Quit();
        }

        private bool ElementIsVisible(IWebElement webElement, int seconds, int pollingFreq)
        {
            IClock clock = new SystemClock();
            var wait = new WebDriverWait(clock, _driver, TimeSpan.FromSeconds(seconds), TimeSpan.FromSeconds(pollingFreq));
            var isVisible = wait.Until(condition =>
            {
                try
                {
                    return webElement.Displayed;
                }

                catch (StaleElementReferenceException)
                {
                    return false;
                }

                catch (NoSuchElementException)
                {
                    return false;
                }
            });

            return isVisible;
        }
    }
}